# cs_ros_bridge - A simple C API on top of roscpp #

cs_ros_bridge has been created to allow ROS to be used from within Unity and Visual C#, particularly on Android and Windows. In order to do this, a set of C bindings are generated (in a similar way to rosserial), and compiled into a SO/DLL. The .NET FFI is then used to call functions in the DLL.

cs_ros_bridge has been developed as part of the OPAL project at the University of Queensland (http://www.itee.uq.edu.au/research/projects/opal), which has been funded by the ARC Centre of Excellence for the Dynamics of Language (http://www.dynamicsoflanguage.edu.au/).

### Dependencies ###

There are a number of dependencies for building this project manually.

* Android NDK (I have not checked if certain versions do or do not work, the version used in this repository is android-ndk-r15b)

* ROS for Android NDK (see here https://github.com/ekumenlabs/roscpp_android). I have taken a selection of binary files from an Android build and placed them in this repository (https://gitlab.com/scott_ch/cs_ros_bridge/blob/master/catkin_ws/devel/roscpp_android_lite.zip)

* Lua (see https://www.lua.org/). lua5.2 is used by this repository.

* lua-fileystem (see https://keplerproject.github.io/luafilesystem/).

* SLT2. A templating library for lua (see https://github.com/henix/slt2). Included in the repository.

* Lake. A simple build system written in lua (see https://github.com/stevedonovan/Lake). Included in the repository.

### Building using Docker ###

It should be possible to take the .gitlab-ci and run it on a Ubuntu machine using gitlab-runner, but I have not tried that.

### Building by forking ###

Using Gitlab's CI, this repository builds itself. One can fork the repository, change some of the files and Gitlab will build it.

### Building locally ###

* Use Ubuntu (good luck on Windows ...).

* Install the Android NDK

* Install lua and lua-filesystem

```bash
sudo apt install lua5.2 lua-filesystem
```

* Run ./build.sh BUILD_ANDROID=true ANDROID_NDK_ROOT="/path/to/android/sdk"


### Adding messages ###

* Edit this line in lakefile

```lua
-- The messages to use
if CS_ROS_PACKAGES == nil then CS_ROS_PACKAGES='std_msgs|geometry_msgs|sensor_msgs|rubi_msgs|phidgets' end
```

to

```lua
if CS_ROS_PACKAGES == nil then CS_ROS_PACKAGES='std_msgs|geometry_msgs|sensor_msgs|rubi_msgs|phidgets|<new_msg_package>' end
```

* Add package (with msgs in msg folder) to catkin_ws/src/<package_name>. Messages need to be in  catkin_ws/src/<package_name>/msg

* Add C++ generated files (from catkin build) to catkin_ws/devel/include/<package_name>

* Rebuild the project.


### Using cs_ros_bridge ###

cs_ros_bridge (for Unity on Android) consists of a shared object (libcs_ros_bridge.so), and a C# interface (ROS.cs). Both files are build using Gitlab's CI, so you can download them directly here: https://gitlab.com/scott_ch/cs_ros_bridge/-/jobs/artifacts/master/download?job=build

Add ROS.cs to your Unity project directly, then add the shared object (libcs_ros_bridge.so) under Plugins/libs/armeabi-v7a.

Initialize using the following code:

```csharp

IEnumerator cs_ros_bridge_init()
{
  string topic_root = "/my_topic_root";
  string node_name = "my_node_name";
  string subnet = "192.168.0";
  string master_uri = "http://"+subnet+".2:11311";

  Debug.Log("Initialising cs_ros_bridge");

  if (!ROS.check_version())
  {
    Debug.LogWarning("Version mismatch! ROS.cs is different version to libcs_ros_bridge.so");

    // handle error here
    // but it is not always fatal, it usually means that
    // the compiled messages are different.
  }

  if (!ROS.is_inited)
  {
    IPAddress address = ROS.GetLocalIPAddress();
    if (address == null || !address.ToString().StartsWith(subnet))
    {
      // IP address we have got is invalid
      // handle error
      return;
    }

    ROS.init_bg(master_uri, address.ToString(), node_name);
  }

  if (ROS.is_inited && !ROS.is_connected)
  {
    while (!ROS.init_bg_update())
    {
      yield;
    }

    if (ROS.is_connected)
    {
      Debug.Log("ROS connected");
    }
  }

  ROS.NodeHandle node = new ROS.NodeHandle();

  // setup ROS topics here - or check for ROS.is_connected later


  while (true)
  {
    if (!ROS.spin_once())
    {
      // node requested to shutdown
      // handle here.
    }

    yield;
  }
}
```

Then add to the Start function in one of the Unity scripts.

```csharp
StartCoroutine(cs_ros_bridge_init);
```

To advertise and publish on a ROS topic (example for a std_msgs/Image):
```csharp
// within some MonoBehavior derived class

class Something : public MonoBehavior
{

private ROS.NodeHandle node = null;
private ROS.Publisher image_pub = null;
private IntPtr image_msg = IntPtr.Zero;
private IntPtr image_msg_ptr = IntPtr.Zero;

private System.Drawing.Imaging.Bitmap bitmap = null;
private System.Drawing.Imaging.BitmapData bitmap_data = null;

void Start()
{
  bitmap = new Bitmap(new Bitmap("/sdcard/DCIM/Camera/image.jpg"))
    .Clone(new Rectangle(0, 0, 640, 480), PixelFormat.Format32bppArgb);
}

void Update()
{

  if (ROS.is_connected && node == null)
  {
    // intialisation

    node = new ROS.NodeHandle();

    image_pub = node.advertise("/my/amazing/image/topic", // topic name
      1, // queue size
      ROS.cs_ros_message_Image_publisher_ctor // message type (via publisher_ctor)
    );

    // Note that creating a message only once will only work for messages sent
    // to the network, _not_ for publishers / subscribers running in the same
    // process (see http://wiki.ros.org/roscpp/Internals "No-Copy Intraprocess Support").
    IntPtr image_msg = ROS.cs_ros_message_Image_create();
    IntPtr image_msg_ptr = ROS.cs_ros_message_Image_get_pointer();
  }

  if (ROS.is_connected && node != null)
  {
    // fill in message fields (see definition of std_msgs/Image)
    // http://docs.ros.org/api/sensor_msgs/html/msg/Image.html


    // set header properties ---------------------------------------------
    IntPtr header_ptr = ROS.cs_ros_message_Image_get_pointer_header(image_msg_ptr);

    // increment the seq (this only works for messages that are created once)
    ROS.cs_ros_message_Header_set_seq(header_ptr, ROS.cs_ros_message_Header_get_seq(header_ptr) + 1);

    // set the timestamp to now
    ROS.cs_ros_time_set_now(ROS.cs_ros_message_Header_get_pointer_stamp(header_ptr));

    // set the frame id
    ROS.cs_ros_string_set(ROS.cs_ros_message_Header_get_pointer_frame_id(header_ptr), "myframeid");
    // ---------------------------------------------------------------------



    // set image properties -------------------------------------------------
    ROS.cs_ros_message_Image_set_width(image_msg_ptr, 640);
    ROS.cs_ros_message_Image_set_height(image_msg_ptr, 480);

    ROS.cs_ros_string_set(ROS.cs_ros_message_Image_get_pointer_encoding(image_msg_ptr), "bgra8");

    ROS.cs_ros_message_Image_set_is_bigendian(image_msg_ptr, 1);

    ROS.cs_ros_message_Image_set_step(image_msg_ptr, 8 * 4 * 640);

    // this can be done once at intialisation if preferred.
    ROS.cs_ros_message_Image_resize_data(image_msg_ptr, 8 * 4 * 640 * 480);

    IntPtr image_data = ROS.cs_ros_message_Image_get_array_pointer_data(image_msg_ptr);

    bitmap_data =
        bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
        bitmap.PixelFormat);

    Marshal.Copy(bitmap_data.Scan0, 0, image_data, 8 * 4 * 640 * 480);

    bitmap.UnlockBits(bitmap_data);
    // ----------------------------------------------------------------------


    // publish message
    image_pub.publish(image_msg);

  }

}

}
```

To subscribe to a topic:
```csharp
// within some MonoBehavior derived class

class SomethingElse : public MonoBehavior
{

private ROS.NodeHandle node = null;
private ROS.Subscriber battery_sub = null;


private void battery_state_callback(IntPtr message)
{
  // this example is around sensor_msgs/BatteryState
  // (see http://docs.ros.org/api/sensor_msgs/html/msg/BatteryState.html)

  // note: all ROS callback are called from within ROS.spin_once() (In the same thread too).

  // get the pointer to the message
  IntPtr message_ptr = ROS.cs_ros_message_BatteryState_get_pointer(message);

  // get the header information --------------------------------------
  IntPtr header_ptr = ROS.cs_ros_message_BatteryState_get_pointer_header(message_ptr);
  System.UInt32 seq = ROS.cs_ros_message_Header_get_seq(header_ptr);

  System.UInt32 stamp_sec, stamp_nsec;
  IntPtr stamp_ptr = ROS.cs_ros_message_Header_get_pointer_stamp(header_ptr);
  stamp_sec = ROS.cs_ros_time_get_sec(stamp_ptr);
  stamp_nsec = ROS.cs_ros_time_get_nsec(stamp_ptr);

  string frame_id = ROS.cs_ros_string_get_as_string(ROS.cs_ros_message_Header_get_pointer_frame_id(header_ptr));
  // ------------------------------------------------------------------



  // get the fields ---------------------------------------------------
  float voltage = ROS.cs_ros_message_BatteryState_get_voltage(message_ptr);
  float current = ROS.cs_ros_message_BatteryState_get_current(message_ptr);
  float charge = ROS.cs_ros_message_BatteryState_get_charge(message_ptr);
  float capacity = ROS.cs_ros_message_BatteryState_get_capacity(message_ptr);
  float design_capacity = ROS.cs_ros_message_BatteryState_get_design_capacity(message_ptr);
  float percentage = ROS.cs_ros_message_BatteryState_get_percentage(message_ptr);

  System.Byte power_supply_status = ROS.cs_ros_message_BatteryState_get_power_supply_status(message_ptr);
  System.Byte power_supply_health = ROS.cs_ros_message_BatteryState_get_power_supply_health(message_ptr);
  System.Byte power_supply_technology = ROS.cs_ros_message_BatteryState_get_power_supply_technology(message_ptr);

  // note: booleans come back as bytes, due to the C-style API
  System.Byte present = ROS.cs_ros_message_BatteryState_get_present(message_ptr);

  System.UInt32 num_cells = ROS.cs_ros_message_BatteryState_get_size_cell_voltage(message_ptr);

  float [] cell_voltage = new float[num_cells];

  for (System.UInt32 i = 0; i < num_cells; i++)
  {
    cell_voltage[i] = ROS.cs_ros_message_BatteryState_get_array_member_cell_voltage(message_ptr, i);
  }

  string location = ROS.cs_ros_string_get_as_string(ROS.cs_ros_message_BatteryState_get_pointer_location(message_ptr));
  string serial_number = ROS.cs_ros_string_get_as_string(ROS.cs_ros_message_BatteryState_get_pointer_serial_number(message_ptr));
  // ------------------------------------------------------------------


  // do something with the fields -------------------------------------
  Console.Log("Voltage: " + voltage);
  Console.Log("Current: " + current);
  Console.Log("Charge: " + charge);
  Console.Log("Capacity: " + capacity);
  Console.Log("Design capacity: " + design_capacity);
  Console.Log("Percentage: " + percentage);


  // Constants accessed through their own functions.
  if (power_supply_status == ROS.cs_ros_message_BatteryState_get_POWER_SUPPLY_STATUS_UNKNOWN())
  {
    Console.Log("Power supply status: Unknown");
  }
  else if (power_supply_status == ROS.cs_ros_message_BatteryState_get_POWER_SUPPLY_STATUS_CHARGING())
  {
    Console.Log("Power supply status: Charging");
  }
  else if (power_supply_status == ROS.cs_ros_message_BatteryState_get_POWER_SUPPLY_STATUS_DISCHARGING())
  {
    Console.Log("Power supply status: Discharging");
  }
  else if (power_supply_status == ROS.cs_ros_message_BatteryState_get_POWER_SUPPLY_STATUS_NOT_CHARGING())
  {
    Console.Log("Power supply status: Not charging");
  }
  else if (power_supply_status == ROS.cs_ros_message_BatteryState_get_POWER_SUPPLY_STATUS_FULL())
  {
    Console.Log("Power supply status: Full");
  }

  // etc ...
  // booleans as a byte are 1 for true, 0 for false
  if (present == 1)
  {
    Console.Log("Battery is present");
  }
  else
  {
    Console.Log("Battery is not present");
  }
  // ------------------------------------------------------------------
}

void Update()
{

  if (ROS.is_connected && node == null)
  {
    // intialisation

    node = new ROS.NodeHandle();

    // create callback object and closure to set member function as callback
    ROS.MessageCallback callback_handle = new ROS.MessageCallback((IntPtr message) => this.battery_state_callback(message));

    // *Extremely important*, do not let the callback object get garbage collected!
    // Results are random otherwise.
    // This line is technically a memory leak, but if the callback is intended to exist for the
    // entire duration of the program, that is not an issue.
    // Also, this can be changed by instead making callback_handle a class member, or by
    // saving the return value of this function somewhere, then calling GCHandle.Free on the variable.
		GCHandle.Alloc(callback_handle);

    battery_sub = node.subscribe("/my/amazing/image/topic", // topic name
      1, // queue size
      callback_handle, // the callback
      ROS.cs_ros_message_BatteryState_subscriber_ctor // message type (via subscriber_ctor)
    );
  }

  if (ROS.is_connected)
  {
    if (!ROS.spin_once())
    {
      // node requested to shutdown
      // handle here.
    }
  }

}

}
```

### Limitations ###

* Only a small subset of ROS is currently supported. Only Publishing / subscribing on topics is supported. ROS services are not supported (we do not have a use-case for them at this point).
* This library only wraps the ROS client roscpp, no ports of rospy / roslaunch / roscore are provided (although there are other projects that aim to do this).
* There are still several ways in which the library could be improved: better type support for C and C# (instead of so many IntPtr / void * variables), addition of faster APIs for accessing or setting all members, etc.
