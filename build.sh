#!/bin/bash

LUA=lua
MD5SUM=md5sum
UNZIP=unzip

# test for lua

echo "Testing for lua:"
$LUA -v

if [ $? != 0 ]; then
  echo "Install lua and lua-filesystem to continue"
  echo "on Ubuntu: sudo apt install lua lua-filesystem"
  exit
fi


# test for lua-filesystem

echo "Testing for lua-filesystem:"
$LUA -e "require 'lfs'" 2> /dev/null

if [ $? != 0 ]; then
  echo "Install lua-filesystem to continue"
  echo "on Ubuntu: sudo apt install lua-filesystem"
  exit
else
  echo "Found lua-filesystem"
fi


# test for md5sum

echo "Testing for md5sum"
$MD5SUM --version

if [ $? != 0 ]; then
  echo "Install md5sum to continue"
  exit
else
  echo "Found md5sum"
fi

# test for md5sum

echo "Testing for unzip"
$UNZIP -v | head -1

if [ $? != 0 ]; then
  echo "Install unzip to continue"
  echo "on Ubuntu: sudo apt install unzip"
  exit
fi

# rest of the build process is through lake, see lakefile in the same directory

$LUA catkin_ws/src/cs_ros_bridge/scripts/lake "$@"
