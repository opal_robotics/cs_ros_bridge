
#ifndef CS_ROS_MESSAGES_H_
#define CS_ROS_MESSAGES_H_

#include "cs_ros_types.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#{
_G.CS_ROS_CALLING_CONVENTION = CS_ROS_CALLING_CONVENTION
_G.CS_ROS_EXPORT_CONVENTION = CS_ROS_EXPORT_CONVENTION
_G.CS_ROS_PACKAGES = CS_ROS_PACKAGES
}#

#{
--local messages = {}
--local handle = io.popen("rosmsg list | grep \"" .. CS_ROS_PACKAGES .. "\"")
--for line in handle:lines() do
--  local package = string.match(line, "%S+/"):sub(1, -2)
--  local filename = string.match(line, "/%S+$"):sub(2, -1)
--  local msg = message_parser.get_message_table(message_parser.rospkg_get_package_path(package) .. "/msg/" .. filename .. ".msg", package)
--  messages[msg.name] = {name = msg.name, namespace = msg.namespace, members = msg.members}
--end

local messages = json.decode(io.open("../../../../build/cs_ros_bridge/json/messages.json", "r"):read("*all"))

}#

#{ for k, message_properties in pairs(messages) do }#

#{
local message_name = message_properties.name;
local message_namespace = message_properties.namespace;
local message_members = message_properties.members;
local msg_prefix =  "cs_ros_message_" .. message_name;
}#

// ----------- begin message #{= message_name }# -------------------------------

// constructor
#{= cs_ros_api_declaration("CS_ROS_Message *", "cs_ros_message_" .. message_name .. "_create", {}) }#

// destructor
#{= cs_ros_api_declaration("void", "cs_ros_message_" .. message_name .. "_destroy", {"CS_ROS_Message * message"}) }#

// copy
#{= cs_ros_api_declaration("CS_ROS_Message *", "cs_ros_message_" .. message_name .. "_copy", {"CS_ROS_Message * message_to_copy"}) }#

#{= cs_ros_api_declaration("void *", "cs_ros_message_" .. message_name .. "_get_copy_ctor", {}) }#

// publisher constructor
#{= cs_ros_api_declaration("CS_ROS_Publisher *", "cs_ros_message_" .. message_name .. "_publisher_ctor", {"CS_ROS_Node_Handle *", "const char * topic", "int queue_size"}) }#

// subscriber constructor
#{= cs_ros_api_declaration("CS_ROS_Subscriber *", "cs_ros_message_" .. message_name .. "_subscriber_ctor", {"CS_ROS_Node_Handle *", "const char * topic", "CS_ROS_Message_Callback callback", "void * user_data", "int queue_size"}) }#

// for languages without pointers
extern CS_ROS_Publisher_Constructor cs_ros_message_#{= message_name }#_publisher_ctor_ptr;

extern CS_ROS_Subscriber_Constructor cs_ros_message_#{= message_name }#_subscriber_ctor_ptr;

#{= cs_ros_api_declaration("CS_ROS_Publisher_Constructor", "cs_ros_message_" .. message_name .. "_get_publisher_ctor", {}) }#

#{= cs_ros_api_declaration("CS_ROS_Subscriber_Constructor", "cs_ros_message_" .. message_name .. "_get_subscriber_ctor", {}) }#



// get the message payload
#{= cs_ros_api_declaration("void *", "cs_ros_message_" .. message_name .. "_get_pointer", {"CS_ROS_Message * message"}) }#

#{ for k2, member_properties in pairs(message_members) do }#

#{
  local member_name = member_properties.name
  local member_type = member_properties.type
  local member_primitive = member_properties.primitive
  local member_array_type = member_properties.arraytype
  local member_const_type = member_properties.consttype
}#

// ----------- begin member #{= message_name }#.#{= member_name }# -------------

#{ if member_primitive and member_array_type == "none" then }#


#{ if member_const_type == "none" then }#

// getter
#{= cs_ros_api_declaration(member_type, "cs_ros_message_" .. message_name .. "_get_" .. member_name, {"void * message_pointer"}) }#


// setter
#{= cs_ros_api_declaration("void", "cs_ros_message_" .. message_name .. "_set_" .. member_name, {"void * message_pointer", member_type .. " value"}) }#


#{ else -- const type }#

extern #{= member_type }# CS_ROS_MESSAGE_#{= message_name }#_#{= member_name }#;

#{= cs_ros_api_declaration(member_type, "cs_ros_message_" .. message_name .. "_get_" .. member_name, {}) }#

#{ end -- const type }#
#{ end -- is primitive }#

#{ if member_const_type == "none" then }#

#{ if member_array_type == "none" then }#

// get pointer
#{= cs_ros_api_declaration(member_type .. " *" , "cs_ros_message_" .. message_name .. "_get_pointer_" .. member_name, {"void * message_pointer"}) }#

#{ else -- member_array_type }#

// get pointer (for array)
#{= cs_ros_api_declaration(member_type .. " *" , "cs_ros_message_" .. message_name .. "_get_array_pointer_" .. member_name, {"void * message_pointer"}) }#

// get size of array
#{= cs_ros_api_declaration("uint32_t" , "cs_ros_message_" .. message_name .. "_get_size_" .. member_name, {"void * message_pointer"}) }#

#{ if member_array_type == "dynamic" then }#
// set size of array
#{= cs_ros_api_declaration("void" , "cs_ros_message_" .. message_name .. "_resize_" .. member_name, {"void * message_pointer", "uint32_t size"}) }#

#{ end -- dynamic array }#

#{ if member_primitive then }#

#{= cs_ros_api_declaration(member_type, "cs_ros_message_" .. message_name .. "_get_array_member_" .. member_name, {"void * message_pointer", "int index"}) }#

#{= cs_ros_api_declaration("void", "cs_ros_message_" .. message_name .. "_set_array_member_" .. member_name, {"void * message_pointer", "int index", member_type .. " value"}) }#


#{ end -- member_primitive }#

#{= cs_ros_api_declaration(member_type .. " *" , "cs_ros_message_" .. message_name .. "_get_array_member_pointer_" .. member_name, {"void * message_pointer", "int index"}) }#


#{ end -- member_array_type }#


#{ end -- const type }#

// ----------- end member #{= message_name }#.#{= member_name }# ---------------
#{ end -- end of member }#

// ----------- end of message #{= message_name }# ------------------------------
#{ end -- end of message }#


#ifdef __cplusplus
}
#endif



#endif
