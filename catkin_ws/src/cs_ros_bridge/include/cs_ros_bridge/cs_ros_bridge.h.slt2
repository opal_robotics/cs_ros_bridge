/*
 * This file defines the standard message-agnostic cs_ros_bridge API.
 */

#ifndef CS_ROS_BRIDGE_H_
#define CS_ROS_BRIDGE_H_

#include "cs_ros_types.h"


#ifdef __cplusplus
extern "C" {
#endif

#{
_G.CS_ROS_CALLING_CONVENTION = CS_ROS_CALLING_CONVENTION
_G.CS_ROS_EXPORT_CONVENTION = CS_ROS_EXPORT_CONVENTION
}#


/*
 * Construct the arguments map and add key value
 * pairs.
 * Arg1 The key to add
 * Arg2 the value to add
 */
#{= cs_ros_api_declaration("void", "cs_ros_argument", {"const char * key", "const char * value"}) }#

/*
 * Initialize ROS.
 * Arg1 the node name to use.
 */
#{= cs_ros_api_declaration("void", "cs_ros_init", {"const char * node_name"}) }#

/*
 * Alternate initialisation - init using a background thread.
 * cs_ros_bgi_create starts the process
 * Arg1 the node name to use.
 */
#{= cs_ros_api_declaration("CS_ROS_Background_Initialiser *", "cs_ros_bgi_create", {"const char * node_name"}) }#

/*
 * Alternate initialisation - init using a background thread.
 * cs_ros_bgi_check checks whether the intialisation is complete
 * Arg1 the background intitialiser returned by cs_ros_bgi_create
 */
#{= cs_ros_api_declaration("uint8_t", "cs_ros_bgi_check", {"CS_ROS_Background_Initialiser * background_initialiser"}) }#

/*
 * Alternate initialisation - init using a background thread.
 * cs_ros_bgi_get_node_handle return the intitial node handle. Creation of a node handle is required to complete the intialisation.
 * Arg1 the background intitialiser returned by cs_ros_bgi_create
 */
#{= cs_ros_api_declaration("CS_ROS_Node_Handle *", "cs_ros_bgi_get_node_handle", {"CS_ROS_Background_Initialiser * background_initialiser"}) }#

/*
 * Alternate initialisation - init using a background thread.
 * cs_ros_bgi_join join the thread used for intialisation.
 * Arg1 the background intitialiser returned by cs_ros_bgi_create
 */
#{= cs_ros_api_declaration("void", "cs_ros_bgi_join", {"CS_ROS_Background_Initialiser * background_initialiser"}) }#

/*
 * Alternate initialisation - init using a background thread.
 * cs_ros_bgi_destroy destroys the initialiser object and thread.
 * *NOTE* if the node handle has not been acquired with cs_ros_bgi_get_node_handle, there will be a memory leak.
 * Arg1 the background intitialiser returned by cs_ros_bgi_create
 */
#{= cs_ros_api_declaration("void", "cs_ros_bgi_destroy", {"CS_ROS_Background_Initialiser * background_initialiser"}) }#

/*
 * Check for a connection to the master
 * Returns 1 for true, 0 for false.
 */
#{= cs_ros_api_declaration("uint8_t", "cs_ros_check_master", {}) }#

/*
 * Create a node handle.
 * This call _BLOCKS_.
 */
#{= cs_ros_api_declaration("CS_ROS_Node_Handle *", "cs_ros_node_handle_create", {}) }#

/*
 * Shutdown ROS
 * Arg1 the node handle created by init.
 */
#{= cs_ros_api_declaration("void", "cs_ros_node_handle_destroy", {"CS_ROS_Node_Handle * node_handle"}) }#

/*
 * Check the status of the node and execute message callbacks
 * within ROS.
 * Arg1 the node handle from init.
 */
#{= cs_ros_api_declaration("CS_ROS_Bool", "cs_ros_spin_once", {"CS_ROS_Node_Handle * node_handle"}) }#

/*
 * Subscribe to a topic.
 * Arg1 the node_handle from init
 * Arg2 the topic name
 * Arg3 the incoming queue size, usually 0 (= infinite) or 1
 * Arg4 a function pointer to a message callback (of the form "void callback (CS_ROS_Message * message)")
 * Arg5 a constructor for a subscriber for a particular message type. Of
 *      the form cs_ros_message_{message_type}_subscriber_ctor
 */
#{= cs_ros_api_declaration("CS_ROS_Subscriber *", "cs_ros_subscribe", {"CS_ROS_Node_Handle * node_handle", "const char * topic", "int queue_size", "CS_ROS_Message_Callback callback", "void * user_data", "CS_ROS_Subscriber_Constructor subscriber_constructor"}) }#


/*
 * Unsubscribe
 * Arg1 the subscriber from cs_ros_subscribe.
 */
#{= cs_ros_api_declaration("void", "cs_ros_unsubscribe", {"CS_ROS_Subscriber * subscriber"}) }#

/*
 * Advertise a topic to publish on.
 * Arg1 node_handle from init
 * Arg2 topic name
 * Arg3 outgoing queue_size (again usually 0 (infinte) or 1)
 * Arg4 publisher constructor of the form
 *      cs_ros_message_{message_type}_publisher_ctor
 */
#{= cs_ros_api_declaration("CS_ROS_Publisher *", "cs_ros_advertise", {"CS_ROS_Node_Handle * node_handle", "const char * topic", "int queue_size", "CS_ROS_Publisher_Constructor constructor"}) }#

/*
 * Remove an advertised topic.
 * Arg1 the publisher from cs_ros_advertise.
 */
#{= cs_ros_api_declaration("void", "cs_ros_unadvertise", {"CS_ROS_Publisher * publisher"}) }#

/*
 * Publish a message.
 * Arg1 the publisher from cs_ros_advertise
 * Arg2 the message (create an empty message with "cs_ros_message_{message_type}_create()"
 *      update its fields with "cs_ros_message_{message_type}_set_{member_name}( value )")
 */
#{= cs_ros_api_declaration("void", "cs_ros_publish", {"CS_ROS_Publisher * publisher", "CS_ROS_Message * message"}) }#

/*
 * Utility to get the time in seconds.
 */
#{= cs_ros_api_declaration("double", "cs_ros_time_now_sec", {}) }#

/*
 * Utility to get high-resolution time.
 * Arg1 output pointer to an integer to hold the seconds
 * Arg2 output pointer to an integer to hold the nano seconds
 */
#{= cs_ros_api_declaration("void", "cs_ros_time_now", {"unsigned int * sec, unsigned int * nsec"}) }#


// handle ROS types string, Time and Duration
#{= cs_ros_api_declaration("const char *", "cs_ros_string_get", {"CS_ROS_String * s"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_string_set", {"CS_ROS_String * s", "const char * value"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_string_destroy", {"CS_ROS_String * s"}) }#
#{= cs_ros_api_declaration("uint32_t", "cs_ros_time_get_sec", {"CS_ROS_Time * t"}) }#
#{= cs_ros_api_declaration("uint32_t", "cs_ros_time_get_nsec", {"CS_ROS_Time * t"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_time_set_sec", {"CS_ROS_Time * t", "uint32_t value"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_time_set_nsec", {"CS_ROS_Time * t",  "uint32_t value"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_time_set_now", {"CS_ROS_Time * t"}) }#
#{= cs_ros_api_declaration("double", "cs_ros_time_to_sec", {"CS_ROS_Time * t"}) }#
#{= cs_ros_api_declaration("uint64_t", "cs_ros_time_to_nsec", {"CS_ROS_Time * t"}) }#
#{= cs_ros_api_declaration("int32_t", "cs_ros_duration_get_sec", {"CS_ROS_Duration * t"}) }#
#{= cs_ros_api_declaration("int32_t", "cs_ros_duration_get_nsec", {"CS_ROS_Duration * t"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_duration_set_sec", {"CS_ROS_Duration * t", "int32_t value"}) }#
#{= cs_ros_api_declaration("void", "cs_ros_duration_set_nsec", {"CS_ROS_Duration * t",  "int32_t value"}) }#
#{= cs_ros_api_declaration("double", "cs_ros_duration_to_sec", {"CS_ROS_Duration * t"}) }#
#{= cs_ros_api_declaration("int64_t", "cs_ros_duration_to_nsec", {"CS_ROS_Duration * t"}) }#

#{= cs_ros_api_declaration("void", "cs_ros_param_set", {"const char * key", "const char * value"}) }#
#{= cs_ros_api_declaration("CS_ROS_String *", "cs_ros_param_get", {"const char * key"}) }#


#ifdef __cplusplus
}
#endif


#endif
