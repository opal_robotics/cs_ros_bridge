
/*
** The file ROS.cs is generated from ROS.cs.slt2 using the lua template library
** slt2. Please do not edit ROS.cs, instead edit ROS.cs.slt2
*/

using System;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;

#{
_G.CS_ROS_CALLING_CONVENTION = CS_ROS_CALLING_CONVENTION
_G.CS_ROS_EXPORT_CONVENTION = CS_ROS_EXPORT_CONVENTION
_G.CS_ROS_PACKAGES = CS_ROS_PACKAGES
_G.CS_ROS_VERSION_FILENAMES = CS_ROS_VERSION_FILENAMES
}#

#{ for i, bits in ipairs({64, 32, 16}) do }#
using uint#{= bits }#_t = System.UInt#{= bits }#;
using int#{= bits }#_t = System.Int#{= bits }#;
#{ end }#
using int8_t = System.SByte;
using uint8_t = System.Byte;


#{
-- generate versions
CS_ROS_VERSION = {}
for i, filename in ipairs(_G.CS_ROS_VERSION_FILENAMES) do
  file = io.open(filename, "r")
  table.insert(CS_ROS_VERSION, file:read("*line"))
  file:close()
end
}#


public class ROS
{
	public static string DEFAULT_MASTER_URI = "http://192.168.0.2:11311";

	public static uint32_t CS_ROS_VERSION_MAJOR = #{= CS_ROS_VERSION[1] }#;
	public static uint32_t CS_ROS_VERSION_MINOR = #{= CS_ROS_VERSION[2] }#;
	public static string CS_ROS_VERSION_COMPILE = "#{= CS_ROS_VERSION[3] }#";
	public static string CS_ROS_VERSION_VAR = "#{= CS_ROS_VERSION[4] }#";

	// cs_ros_bridge.dll API ---------------------------------------------------

	[DllImport ("cs_ros_bridge")]
	private extern static void cs_ros_argument (string key, string value);

	[DllImport ("cs_ros_bridge")]
	private extern static void cs_ros_init (string node_name);

  [DllImport ("cs_ros_bridge")]
  private extern static IntPtr cs_ros_bgi_create (string node_name);

  [DllImport ("cs_ros_bridge")]
  private extern static uint8_t cs_ros_bgi_check (IntPtr background_initialiser);

  [DllImport ("cs_ros_bridge")]
  private extern static IntPtr cs_ros_bgi_get_node_handle (IntPtr background_initialiser);

  [DllImport ("cs_ros_bridge")]
  private extern static void cs_ros_bgi_join (IntPtr background_initialiser);

  [DllImport ("cs_ros_bridge")]
  private extern static void cs_ros_bgi_destroy (IntPtr background_initialiser);

	[DllImport ("cs_ros_bridge")]
	private extern static uint8_t cs_ros_check_master ();

	[DllImport ("cs_ros_bridge")]
	private extern static IntPtr cs_ros_node_handle_create ();

	[DllImport ("cs_ros_bridge")]
	private extern static void cs_ros_node_handle_destroy (IntPtr node_handle);

	[DllImport ("cs_ros_bridge")]
	private extern static int cs_ros_spin_once (IntPtr node_handle);

	public delegate void MessageCallback(IntPtr message);

	public delegate IntPtr SubscriberConstructor(IntPtr node_handle, IntPtr topic,
	                                             MessageCallback callback, IntPtr user_data,
	                                             int queue_size);

  public delegate IntPtr PublisherConstructor(IntPtr node_handle, IntPtr topic, int queue_size);

  [DllImport ("cs_ros_bridge")]
	private extern static IntPtr cs_ros_subscribe (IntPtr node_handle, string topic, int queue_size,
	                                               MessageCallback callback, IntPtr user_data,
	                                               SubscriberConstructor subscriber_constructor);

	[DllImport ("cs_ros_bridge")]
	private extern static void cs_ros_unsubscribe (IntPtr subscriber);

  [DllImport("cs_ros_bridge")]
  private extern static IntPtr cs_ros_advertise(IntPtr node_handle, string topic, int queue_size,
                                             PublisherConstructor publisher_constructor);

  [DllImport("cs_ros_bridge")]
  private extern static void cs_ros_unadvertise(IntPtr publisher);

  [DllImport("cs_ros_bridge")]
  private extern static void cs_ros_publish (IntPtr publisher, IntPtr message);

  [DllImport("cs_ros_bridge")]
	public extern static IntPtr cs_ros_string_get (IntPtr s);

  // converter for string
  public static string cs_ros_string_get_as_string(IntPtr s)
  {
    return Marshal.PtrToStringAnsi(cs_ros_string_get(s));
  }

  [DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_string_set (IntPtr s, string value);

  [DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_string_destroy (IntPtr s);

  [DllImport("cs_ros_bridge")]
  public extern static uint32_t  cs_ros_time_get_sec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static uint32_t  cs_ros_time_get_nsec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_time_set_sec (IntPtr t, uint32_t value);

	[DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_time_set_nsec (IntPtr t, uint32_t value);

	[DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_time_set_now (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static double  cs_ros_time_to_sec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static uint64_t  cs_ros_time_to_nsec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static int32_t  cs_ros_duration_get_sec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static int32_t  cs_ros_duration_get_nsec (IntPtr t);

	[DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_duration_set_sec (IntPtr t, int32_t value);

	[DllImport("cs_ros_bridge")]
  public extern static void  cs_ros_duration_set_nsec (IntPtr t, int32_t value);

	[DllImport("cs_ros_bridge")]
	public extern static double  cs_ros_duration_to_sec (IntPtr t);

	[DllImport("cs_ros_bridge")]
	public extern static int64_t  cs_ros_duration_to_nsec (IntPtr t);


  // parameter server
	[DllImport("cs_ros_bridge")]
	public extern static void cs_ros_param_set (string key, string value);

	[DllImport("cs_ros_bridge")]
	public extern static IntPtr cs_ros_param_get (string key);


	[DllImport("cs_ros_bridge")]
	public extern static uint32_t cs_ros_get_version_major ();

	[DllImport("cs_ros_bridge")]
	public extern static uint32_t cs_ros_get_version_minor ();

	[DllImport("cs_ros_bridge")]
	public extern static IntPtr cs_ros_get_version_compile ();

	[DllImport("cs_ros_bridge")]
	public extern static IntPtr cs_ros_get_version_var ();


  // ----------------------------------------------------------------------


  // eye_movement

  #{
  --local messages = {}
  --local handle = io.popen("rosmsg list | grep \"" .. CS_ROS_PACKAGES .. "\"")
  --for line in handle:lines() do
  --  local package = string.match(line, "%S+/"):sub(1, -2)
  --  local filename = string.match(line, "/%S+$"):sub(2, -1)
  --  local msg = message_parser.get_message_table(message_parser.rospkg_get_package_path(package) .. "/msg/" .. filename .. ".msg", package)
  --  messages[msg.name] = {name = msg.name, namespace = msg.namespace, members = msg.members}
  --end

  local messages = json.decode(io.open("../../../../build/cs_ros_bridge/json/messages.json", "r"):read("*all"))

  }#


	#{ for k, message_properties in pairs(messages) do }#

	#{
	local message_name = message_properties.name;
	local message_namespace = message_properties.namespace;
	local message_members = message_properties.members;
	local msg_prefix =  "cs_ros_message_" .. message_name;
	}#

	// ----------- begin message #{= message_name }# -------------------------------
	// constructor
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_create ();

	// destructor
	[DllImport ("cs_ros_bridge")]
	public extern static void cs_ros_message_#{= message_name }#_destroy (IntPtr message);

	// publisher constructor
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_publisher_ctor(IntPtr node_handle,
	                                                                       IntPtr topic,
	                                                                       int queue_size);
	// subscriber constructor
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_subscriber_ctor(IntPtr node_handle, IntPtr topic,
	                                                                       MessageCallback callback, IntPtr user_data,
	                                                                       int queue_size);
	// get the message payload
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_get_pointer (IntPtr message);



	#{ for k2, member_properties in pairs(message_members) do }#

	#{
	  local member_name = member_properties.name
	  local member_type = member_properties.type
	  local member_primitive = member_properties.primitive
	  local member_array_type = member_properties.arraytype
	  local member_const_type = member_properties.consttype
	}#

	// ----------- begin member #{= message_name }#.#{= member_name }# -------------

	#{ if member_primitive and member_array_type == "none" then }#

	#{ if member_const_type == "none" then }#

	// getter
	[DllImport ("cs_ros_bridge")]
	public extern static #{= member_type }# cs_ros_message_#{= message_name }#_get_#{= member_name }# (IntPtr message);

	// setter
	[DllImport("cs_ros_bridge")]
	public extern static void cs_ros_message_#{= message_name }#_set_#{= member_name }# (IntPtr message, #{= member_type }# value);

	#{ else -- const type }#

	//[DllImport("cs_ros_bridge")]
	//public extern static #{= member_type }# CS_ROS_MESSAGE_#{= message_name }#_#{= member_name }#;

	[DllImport("cs_ros_bridge")]
	public extern static #{= member_type }# cs_ros_message_#{= message_name }#_get_#{= member_name }# ();

	#{ end -- const type }#
	#{ end -- is primitive }#

	#{ if member_const_type == "none" then }#

	#{ if member_array_type == "none" then }#

	// get pointer
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_get_pointer_#{= member_name }# (IntPtr message);

	#{ else -- array type }#

	#{ if member_primitive then }#

	[DllImport ("cs_ros_bridge")]
	public extern static #{= member_type }# cs_ros_message_#{= message_name }#_get_array_member_#{= member_name }#(IntPtr message_pointer, int index);

	[DllImport ("cs_ros_bridge")]
	public extern static void cs_ros_message_#{= message_name }#_set_array_member_#{= member_name }#(IntPtr message_pointer, int index, #{= member_type }# value);

	#{ end -- member_primitive }#

	// get pointer (for array)
	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_get_array_pointer_#{= member_name }#(IntPtr message_pointer);

	// get size of array
	[DllImport ("cs_ros_bridge")]
	public extern static uint32_t cs_ros_message_#{= message_name }#_get_size_#{= member_name }#(IntPtr message_pointer);

	#{ if member_array_type == "dynamic" then }#

	// set size of array
	[DllImport ("cs_ros_bridge")]
	public extern static void cs_ros_message_#{= message_name }#_resize_#{= member_name }#(IntPtr message_pointer, uint32_t size);

	#{ end -- dynamic array }#

	[DllImport ("cs_ros_bridge")]
	public extern static IntPtr cs_ros_message_#{= message_name }#_get_array_member_pointer_#{= member_name }#(IntPtr message_pointer, int index);

	#{ end -- array type }#

	#{ end -- const type }#

	// ----------- end member #{= message_name }#.#{= member_name }# ---------------
	#{ end -- end of member }#

	// ----------- end of message #{= message_name }# ------------------------------
	#{ end -- end of message }#

	public static IntPtr node_handle = IntPtr.Zero;
	public static bool is_inited { get; protected set; }
	public static bool is_connected { get; protected set; }
  public static IntPtr background_initialiser = IntPtr.Zero;

  public static bool check_version()
  {
    bool major_match = cs_ros_get_version_major () == CS_ROS_VERSION_MAJOR;
    bool minor_match = cs_ros_get_version_minor () == CS_ROS_VERSION_MINOR;
    bool compile_match = Marshal.PtrToStringAnsi(cs_ros_get_version_compile()) == CS_ROS_VERSION_COMPILE;
    bool version_match = Marshal.PtrToStringAnsi(cs_ros_get_version_var()) == CS_ROS_VERSION_VAR;


    return major_match && minor_match && compile_match && version_match;
  }

  public static string print_version()
  {
    return "ROS.cs {" + CS_ROS_VERSION_MAJOR + ", " + CS_ROS_VERSION_MINOR + ", " +
        CS_ROS_VERSION_COMPILE + ", " + CS_ROS_VERSION_VAR + "}\n" +
      "libcs_ros_bridge.cs {" + cs_ros_get_version_major () + ", " + cs_ros_get_version_minor() + ", " +
      Marshal.PtrToStringAnsi(cs_ros_get_version_compile()) + ", " + Marshal.PtrToStringAnsi(cs_ros_get_version_var()) + "}";
  }

	public static void init(string master_uri, string ip, string node_name)
	{
    //Environment.SetEnvironmentVariable("ROS_MASTER_URI", master_uri);
    //Environment.SetEnvironmentVariable("ROS_IP", ip);

		cs_ros_argument ("__master", master_uri);
		cs_ros_argument ("__ip", ip);
		cs_ros_init (node_name);
		is_inited = true;
	}

  public static void init_bg(string master_uri, string ip, string node_name)
  {
    //Environment.SetEnvironmentVariable("ROS_MASTER_URI", master_uri);
    //Environment.SetEnvironmentVariable("ROS_IP", ip);

    cs_ros_argument ("__master", master_uri);
    cs_ros_argument ("__ip", ip);
    background_initialiser = cs_ros_bgi_create (node_name);
    is_inited = true;
  }

  public static bool init_bg_update()
  {
    if (cs_ros_bgi_check(background_initialiser) == 1)
    {
      ROS.node_handle = cs_ros_bgi_get_node_handle(background_initialiser);
      ROS.is_connected = true;

      cs_ros_bgi_join(background_initialiser);
      cs_ros_bgi_destroy(background_initialiser);

      return true;
    }

    return false;
  }

	public class NodeHandle
	{
		public NodeHandle()
		{
			if (ROS.node_handle.Equals(IntPtr.Zero))
			{
				ROS.node_handle = cs_ros_node_handle_create();
				ROS.is_connected = true;
			}
		}

		~NodeHandle()
		{
			// cs_ros_shutdown (node_handle);
		}

		public static bool check_master()
		{
			return (cs_ros_check_master() == 1);
		}

		public bool spin_once()
		{
			return (cs_ros_spin_once (node_handle) == 1);
		}

		public Subscriber subscribe(string topic, int queue_size,
	                            	MessageCallback message_callback,
		                            SubscriberConstructor subscriber_constructor)
		{
			return new Subscriber(node_handle, topic, queue_size, message_callback, subscriber_constructor);
		}

    public Publisher advertise(string topic, int queue_size,
            PublisherConstructor publisher_constructor)
    {
        return new Publisher(node_handle, topic, queue_size, publisher_constructor);
    }

	}

	public class Subscriber
	{
		public IntPtr subscriber;

		public Subscriber(IntPtr node_handle, string topic, int queue_size,
		                  MessageCallback message_callback,
		                  SubscriberConstructor subscriber_constructor)
		{
			subscriber = cs_ros_subscribe(node_handle, topic, queue_size, message_callback, IntPtr.Zero, subscriber_constructor);
		}

		~Subscriber()
		{
			cs_ros_unsubscribe (subscriber);
		}

	}

  public class Publisher
  {
      public IntPtr publisher;

      public Publisher(IntPtr node_handle, string topic, int queue_size,
                        PublisherConstructor publisher_constructor)
      {
          publisher = cs_ros_advertise(node_handle, topic, queue_size, publisher_constructor);
      }

      ~Publisher()
      {
          cs_ros_unadvertise(publisher);
      }

      public void publish(IntPtr message)
      {
          cs_ros_publish(publisher, message);
      }

  }

	public static IPAddress GetLocalIPAddress ()
	{
		IPHostEntry host;

		host = Dns.GetHostEntry (Dns.GetHostName ());
		foreach (IPAddress ip in host.AddressList) {
			if (ip.AddressFamily == AddressFamily.InterNetwork && ip.ToString().StartsWith("192.168.0")) { // must be on Opie's subnet
				return ip;
			}
		}

		return null;
	}
}
