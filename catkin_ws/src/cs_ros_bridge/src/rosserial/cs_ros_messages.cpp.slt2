

#include <cs_ros_bridge/cs_ros_bridge.h>
#include <cs_ros_bridge/cs_ros_messages.h>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

#include <iostream>

#define BUILD_LIBROSSERIALEMBEDDEDLINUX
#include <ros.h>
#include "cs_ros_datatypes_private.h"

#{
_G.CS_ROS_CALLING_CONVENTION = CS_ROS_CALLING_CONVENTION
_G.CS_ROS_EXPORT_CONVENTION = CS_ROS_EXPORT_CONVENTION
_G.CS_ROS_PACKAGES = CS_ROS_PACKAGES
}#

#{
--local messages = {}
--local handle = io.popen("rosmsg list | grep \"" .. CS_ROS_PACKAGES .. "\"")
--for line in handle:lines() do
--  local package = string.match(line, "%S+/"):sub(1, -2)
--  local filename = string.match(line, "/%S+$"):sub(2, -1)
--  local msg = message_parser.get_message_table(message_parser.rospkg_get_package_path(package) .. "/msg/" .. filename .. ".msg", package)
--  messages[msg.name] = {name = msg.name, namespace = msg.namespace, members = msg.members}
--end

local messages = json.decode(io.open("../../../../build/cs_ros_bridge/json/messages.json", "r"):read("*all"))

}#

#{ for k, message_properties in pairs(messages) do -- for all the messages we have }#

#{
local message_name = message_properties.name;
local message_namespace = message_properties.namespace;
local message_members = message_properties.members;
local msg_prefix =  "cs_ros_message_" .. message_name;
}#

// include required header
#include<#{= message_namespace }#/#{= message_name }#.h>

// message struct definition
struct CS_ROS_Message_#{= message_name }#
{
  CS_ROS_Message message;
  #{= message_namespace }#::#{= message_name }# #{= message_name }#;
};

// message publisher struct definition
struct CS_ROS_Publisher_#{= message_name }#
{
  CS_ROS_Publisher cs_ros_publisher;
  ros::Publisher * publisher;
  #{= message_namespace }#::#{= message_name }# msg;
};

// message subscriber struct definition
struct CS_ROS_Subscriber_#{= message_name }#
{
  CS_ROS_Subscriber cs_ros_subscriber;
  class Subscriber_ROSSerial_#{= message_name}# * subscriber;
};

// constructor definition
#{= cs_ros_api_definition("CS_ROS_Message *", "cs_ros_message_" .. message_name .. "_create", {}) }#
{
  CS_ROS_Message_#{= message_name }# * message = new CS_ROS_Message_#{= message_name }#;
  message->message.raw = NULL;
  message->message.buffer_size = 0;
  //message->#{= message_name }# = boost::shared_ptr<#{= message_namespace }#::#{= message_name }#>(new #{= message_namespace }#::#{= message_name }#());
  return (CS_ROS_Message*) message;
}

// destructor definition
#{= cs_ros_api_definition("void", "cs_ros_message_" .. message_name .. "_destroy", {"CS_ROS_Message * message"}) }#
{
  CS_ROS_Message_#{= message_name }# * cast_message = (CS_ROS_Message_#{= message_name }# *) message;
  if (cast_message){

    if (cast_message->message.raw)
    {
      delete cast_message->message.raw;
    }

     delete cast_message;
  }
}

// copy
#{= cs_ros_api_definition("CS_ROS_Message *", "cs_ros_message_" .. message_name .. "_copy", {"CS_ROS_Message * message_to_copy"}) }#
{
  // TODO: this function is a hack to support no-callback based reading
  CS_ROS_Message_#{= message_name }# * message = (CS_ROS_Message_#{= message_name }# *) cs_ros_message_#{= message_name }#_create();
  CS_ROS_Message_#{= message_name }# * message_to_copy_cast = (CS_ROS_Message_#{= message_name }# *) message_to_copy;

  message->message.raw = new unsigned char[message_to_copy_cast->message.buffer_size];
  message->message.buffer_size = message_to_copy_cast->#{= message_name }#.serialize(message->message.raw);

  message->#{= message_name }#.deserialize(message->message.raw);

  return (CS_ROS_Message*) message;
}

#{= cs_ros_api_definition("void *", "cs_ros_message_" .. message_name .. "_get_copy_ctor", {}) }#
{
  return (void*) cs_ros_message_#{= message_name }#_copy;
}

// publish definition
#{= cs_ros_api_definition("void", "cs_ros_message_" .. message_name .. "_publish", {"CS_ROS_Publisher * publisher", "CS_ROS_Message * message"}) }#
{
  ((CS_ROS_Publisher_#{= message_name }# *) publisher)->publisher->publish(&(((CS_ROS_Message_#{= message_name }# *)message)->#{= message_name }#));
}

// publisher constructor definition
#{= cs_ros_api_definition("CS_ROS_Publisher *", "cs_ros_message_" .. message_name .. "_publisher_ctor", {"CS_ROS_Node_Handle * node_handle", "const char * topic", "int queue_size"}) }#
{
  CS_ROS_Publisher_#{= message_name }# * publisher = new CS_ROS_Publisher_#{= message_name }#();
  publisher->publisher = new ros::Publisher(topic, &publisher->msg);
  publisher->cs_ros_publisher.publish = &cs_ros_message_#{= message_name }#_publish;
  node_handle->node_handle.advertise(*publisher->publisher);
  return (CS_ROS_Publisher*) publisher;
}

// function pointer (for languages that do not have pointers)
CS_ROS_Publisher_Constructor cs_ros_message_#{= message_name }#_publisher_ctor_ptr = &cs_ros_message_#{= message_name }#_publisher_ctor;

#{= cs_ros_api_definition("CS_ROS_Publisher_Constructor", "cs_ros_message_" .. message_name .. "_get_publisher_ctor", {}) }#
{
  return cs_ros_message_#{= message_name }#_publisher_ctor_ptr;
}

// ROS callback definition (Not API, private function)
/*void #{= message_name }#_callback(boost::shared_ptr<#{= message_namespace }#::#{= message_name }#> message, CS_ROS_Message_Callback callback, CS_ROS_Subscriber_#{= message_name }# * subscriber)
{
  CS_ROS_Message_#{= message_name }# cs_ros_message;
  cs_ros_message.#{= message_name }# = boost::const_pointer_cast<#{= message_namespace }#::#{= message_name }#>(message);
  callback((CS_ROS_Message*)&cs_ros_message, subscriber->cs_ros_subscriber.user_data);
}*/

class Subscriber_ROSSerial_#{= message_name}#: public ros::Subscriber<#{= message_namespace }#::#{= message_name }#> {
public:

  #{= message_namespace }#::#{= message_name }# msg;
  CS_ROS_Message_Callback cb;
  CS_ROS_Subscriber_#{= message_name }# * subscriber;

  Subscriber_ROSSerial_#{= message_name}#(const char * topic_name, CS_ROS_Message_Callback callback,   CS_ROS_Subscriber_#{= message_name }# * subscriber, int endpoint=rosserial_msgs::TopicInfo::ID_SUBSCRIBER) :
    Subscriber(topic_name, NULL, endpoint),
	  cb(callback),
    endpoint_(endpoint),
    subscriber(subscriber)
  {
  	topic_ = topic_name;
  };

  virtual void callback(unsigned char* data)
  {
  	//msg.deserialize(data);
  	CS_ROS_Message_#{= message_name }# cs_ros_message;
    cs_ros_message.message.raw = data;
  	//cs_ros_message.#{= message_name }# = boost::const_pointer_cast<#{= message_namespace }#::#{= message_name }#>(msg);
  	cs_ros_message.message.buffer_size = cs_ros_message.#{= message_name }#.deserialize(data);
  	cb((CS_ROS_Message*)&cs_ros_message, subscriber->cs_ros_subscriber.user_data);
  }

  virtual const char * getMsgType(){ return this->msg.getType(); }
  virtual const char * getMsgMD5(){ return this->msg.getMD5(); }
  virtual int getEndpointType(){ return endpoint_; }

private:
  int endpoint_;
};

// subscriber constructor definition
#{= cs_ros_api_definition("CS_ROS_Subscriber *", "cs_ros_message_" .. message_name .. "_subscriber_ctor", {"CS_ROS_Node_Handle * node_handle", "const char * topic", "CS_ROS_Message_Callback callback", "void * user_data", "int queue_size"}) }#
{
  CS_ROS_Subscriber_#{= message_name }# * subscriber = new CS_ROS_Subscriber_#{= message_name }#();
  subscriber->cs_ros_subscriber.user_data = user_data;
  subscriber->subscriber = new Subscriber_ROSSerial_#{= message_name}#(topic, callback, subscriber);
  node_handle->node_handle.subscribe(*subscriber->subscriber);
  return (CS_ROS_Subscriber*) subscriber;
}

// function pointer (for languages that do not have pointers)
CS_ROS_Subscriber_Constructor cs_ros_message_#{= message_name }#_subscriber_ctor_ptr = &cs_ros_message_#{= message_name }#_subscriber_ctor;

#{= cs_ros_api_definition("CS_ROS_Subscriber_Constructor", "cs_ros_message_" .. message_name .. "_get_subscriber_ctor", {}) }#
{
  return cs_ros_message_#{= message_name }#_subscriber_ctor_ptr;
}

// get message payload
#{= cs_ros_api_definition("void *", "cs_ros_message_" .. message_name .. "_get_pointer", {"CS_ROS_Message * message"}) }#
{
  return (void*) &(((CS_ROS_Message_#{= message_name }# *)message)->#{= message_name }#);
}


#{ for k2, member_properties in pairs(message_members) do }#

#{
  local member_name = member_properties.name
  local member_type = member_properties.type
  local member_primitive = member_properties.primitive
  local member_array_type = member_properties.arraytype
  local member_const_type = member_properties.consttype
}#


#{ if member_primitive and member_array_type == "none" then }#

#{ if member_const_type == "none" then }#

// getter
#{= cs_ros_api_definition(member_type, "cs_ros_message_" .. message_name .. "_get_" .. member_name, {"void * message_pointer"}) }#
{
  return ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#;
}

// setter
#{= cs_ros_api_definition("void", "cs_ros_message_" .. message_name .. "_set_" .. member_name, {"void * message_pointer", member_type .. " value"}) }#
{
  ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }# = value;
}

#{ else -- member_const_type }#

#{= member_type }# CS_ROS_MESSAGE_#{= message_name }#_#{= member_name }# = #{= message_namespace }#::#{= message_name }#::#{= member_name }#;

#{= cs_ros_api_definition(member_type, "cs_ros_message_" .. message_name .. "_get_" .. member_name, {}) }#
{
  return CS_ROS_MESSAGE_#{= message_name }#_#{= member_name }#;
}

#{ end -- member_const_type }#
#{ end -- member_primitve and member_array_type }#

#{ if member_const_type == "none" then }#

#{ if member_array_type == "none" then }#

// get as pointer
#{= cs_ros_api_definition(member_type .. " *" , "cs_ros_message_" .. message_name .. "_get_pointer_" .. member_name, {"void * message_pointer"}) }#
{
  return (#{= member_type .. " *" }#)&(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#);
}

#{ else -- member_array_type }#

// get as pointer (for array)
#{= cs_ros_api_definition(member_type .. " *" , "cs_ros_message_" .. message_name .. "_get_array_pointer_" .. member_name, {"void * message_pointer"}) }#
{
  return (#{= member_type .. " *" }#)&(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[0]);
}


#{ if member_array_type == "dynamic" then }#
// get the array size
#{= cs_ros_api_definition("uint32_t" , "cs_ros_message_" .. message_name .. "_get_size_" .. member_name, {"void * message_pointer"}) }#
{
  //return ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#.size();
  return ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#_length;
}

// set size of array
#{= cs_ros_api_definition("void" , "cs_ros_message_" .. message_name .. "_resize_" .. member_name, {"void * message_pointer", "uint32_t size"}) }#
{
  //((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#.resize(size);
  *((void**)&((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#) = realloc(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#, size*sizeof(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[0]));
  ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#_length = size;
}
#{ else -- dynamic array}#

// get the array size
#{= cs_ros_api_definition("uint32_t" , "cs_ros_message_" .. message_name .. "_get_size_" .. member_name, {"void * message_pointer"}) }#
{
  //return ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#.size();
  return sizeof(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#)/sizeof(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[0]);
}


#{ end -- dynamic array }#

#{ if member_primitive then }#

#{= cs_ros_api_definition(member_type, "cs_ros_message_" .. message_name .. "_get_array_member_" .. member_name, {"void * message_pointer", "int index"}) }#
{
  return (((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[index]);
}

#{= cs_ros_api_definition("void", "cs_ros_message_" .. message_name .. "_set_array_member_" .. member_name, {"void * message_pointer", "int index", member_type .. " value"}) }#
{
  ((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[index] = value;
}

#{ end -- member_primitive }#

#{= cs_ros_api_definition(member_type .. " *", "cs_ros_message_" .. message_name .. "_get_array_member_pointer_" .. member_name, {"void * message_pointer", "int index"}) }#
{
  return (#{= member_type .. " *" }#)&(((#{= message_namespace }#::#{= message_name }# *) message_pointer)->#{= member_name }#[index]);
}




#{ end -- member_array_type }#


#{ end }#

#{ end }# // members
#{ end }# // messages
