
/*
 * An example node using the cs_ros_bridge API
 *
 * This node is written in C, as the
 * API must be C to avoid the C++ name-mangling.
 */



#include <cs_ros_bridge/cs_ros_bridge.h>
#include <cs_ros_bridge/cs_ros_messages.h>

#include <unistd.h>
#include <stdio.h>

/*
 * Callback for a received message.
 */
void eye_movement_callback(CS_ROS_Message * message, void * user_data)
{
  void * message_payload = cs_ros_message_EyeMovement_get_pointer(message);
  void * message_header = cs_ros_message_EyeMovement_get_pointer_header(message_payload);
  //printf("Message at %.02fs\n", cs_ros_message_eye_movement_get_header_stamp_secs(message_payload));
  printf("---------------\n");
  printf("header : %u\n", cs_ros_message_Header_get_seq(message_header));
  CS_ROS_Time * stamp = cs_ros_message_Header_get_pointer_stamp(message_header);
  printf("\tstamp : %u.%09u\n", cs_ros_time_get_sec(stamp), cs_ros_time_get_nsec(stamp));
  printf("x : %.02f\n", cs_ros_message_EyeMovement_get_x(message_payload));
  printf("y : %.02f\n", cs_ros_message_EyeMovement_get_y(message_payload));
  printf("\n");
}

int main(int argc, char * argv[])
{
  /*
   * Parse commandline-style key:=value arguments to ROS before
   * calling init.
   * Arg1 is the key
   * Arg2 is the value
   */
  cs_ros_argument("__master", "http://127.0.0.1:11311");
  cs_ros_argument("__ip", "127.0.0.1");


  /*
   * Initialize the node and attempt to connect to the
   * master. Note: a process is limited to a single node_handle.
   * Arg1 is the node name.
   */
  cs_ros_init("cs_ros_test");

  printf("Searching for master ...");
  while (!cs_ros_check_master())
  {
    printf(".");
    fflush(stdout);

    // sleep for 1s
    usleep(1000000);
  }
  printf("done.\n");

  // try to make the connections
  CS_ROS_Node_Handle * node_handle = cs_ros_node_handle_create();

  /*
   * Advertise a topic.
   * Arg1 is the node handle from init
   * Arg2 is the topic name ("/hello/world")
   * Arg3 is the outgoing queue size (0 = infinite)
   * Arg4 specifies the message type (eye_movement) through a constructor.
   */
  CS_ROS_Publisher * publisher = cs_ros_advertise(node_handle, "/hello/world", 0,
    &cs_ros_message_EyeMovement_publisher_ctor);

  /*
   * Subscribe to a topic
   * Arg1 is the node handle from init
   * Arg2 is the topic name ("/goodbye/world")
   * Arg3 is the incoming queue size (0 = infinite)
   * Arg4 is a callback for a message received.
   * Arg5 specifies the message type (eye_movement) through a constructor.
   */
  CS_ROS_Subscriber * subscriber = cs_ros_subscribe(node_handle, "/goodbye/world", 0,
    &eye_movement_callback, NULL,
    &cs_ros_message_EyeMovement_subscriber_ctor);


  /*
   * Create an empty message to use for publishing.
   */
  CS_ROS_Message * message = cs_ros_message_EyeMovement_create();
  void * message_payload = cs_ros_message_EyeMovement_get_pointer(message);

  unsigned long milliseconds_counter = 0;
  while (cs_ros_spin_once(node_handle) == CS_ROS_TRUE)
  {

    if (milliseconds_counter % 1000 == 0)
    {
      /*
       *  every second set the eye_movement (x, y).
       */
      cs_ros_message_EyeMovement_set_x(message_payload, milliseconds_counter / 1000);
      cs_ros_message_EyeMovement_set_y(message_payload, milliseconds_counter % 1000);

      /*
       * publish the eye_movement message
       */
      cs_ros_publish(publisher, message);
    }

    // sleep for 100ms
    usleep(100000);

    // update the counter
    milliseconds_counter += 100;
  }

  /*
   * Unsubscribe. This is optional if the program is
   * exiting anyway.
   */
  cs_ros_unsubscribe(subscriber);
  cs_ros_unadvertise(publisher);

  /*
   * Shutdown the node handle.
   */
  cs_ros_node_handle_destroy(node_handle);

  return 0;
}
