
/*
 * An example node using the cs_ros_bridge API
 *
 * This node is written in C, as the
 * API must be C to avoid the C++ name-mangling.
 */

#include "cs_ros_bridge_matlab.h"


#include <unistd.h>
#include <stdio.h>

#include <functional>
#include <queue>
#include <iostream>

struct QueuedMessage
{
  CS_ROS_Subscriber_Constructor message_type;
  CS_ROS_Message * message;

  QueuedMessage(CS_ROS_Subscriber_Constructor message_type, CS_ROS_Message * message) :
    message_type(message_type), message(message){}

  QueuedMessage(const QueuedMessage & other) :
    message_type(other.message_type), message(other.message){}
};

static std::queue<QueuedMessage> matlab_message_queue;

static void callback_stub(CS_ROS_Message * message, void * user_data)
{
  auto func = static_cast<std::function<void (CS_ROS_Message *)> *>(user_data);
  (*func)(message);
}

void cs_ros_matlab_queue_subscribe(CS_ROS_Node_Handle * node_handle, const char * topic, int queue_size,
  CS_ROS_Subscriber_Constructor subscriber_constructor, CS_ROS_Message_Copy_Constructor copy_constructor)
{
  CS_ROS_Subscriber * subscriber = cs_ros_subscribe(node_handle, topic, queue_size,
    &callback_stub, new std::function<void (CS_ROS_Message *)>([subscriber_constructor, copy_constructor](CS_ROS_Message * message) {
      CS_ROS_Message * copy = copy_constructor(message);
      matlab_message_queue.push(QueuedMessage(subscriber_constructor, copy));

    }),
    subscriber_constructor);
}

int cs_ros_matlab_spin(CS_ROS_Node_Handle * node_handle)
{
  int counter = 0;
  while (matlab_message_queue.size() == 0 && counter < 5) // 500 ms
  {
    if (cs_ros_spin_once(node_handle) == CS_ROS_FALSE)
    {
      return CS_ROS_MATLAB_SHUTDOWN;
    }

    usleep(100000);
    counter++;
  }

  if (matlab_message_queue.size() == 0)
  {
    return CS_ROS_MATLAB_NO_MESSAGE;
  }
  else
  {
    return CS_ROS_MATLAB_NEW_MESSAGE;
  }
}

CS_ROS_Subscriber_Constructor cs_ros_matlab_queue_read_message_type(CS_ROS_Node_Handle * node_handle)
{
  if (matlab_message_queue.size() == 0)
  {
    return NULL;
  }

  return matlab_message_queue.front().message_type;
}

CS_ROS_Message * cs_ros_matlab_queue_read_message(CS_ROS_Node_Handle * node_handle)
{
  if (matlab_message_queue.size() == 0)
  {
    return NULL;
  }

  return matlab_message_queue.front().message;
}

void cs_ros_matlab_queue_pop()
{
  matlab_message_queue.pop();
}
