
/*
 * An example node using the cs_ros_bridge API
 *
 * This node is written in C, as the
 * API must be C to avoid the C++ name-mangling.
 */



#include <cs_ros_bridge/cs_ros_bridge.h>
#include <cs_ros_bridge/cs_ros_messages.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CS_ROS_MATLAB_SHUTDOWN 1
#define CS_ROS_MATLAB_NO_MESSAGE 2
#define CS_ROS_MATLAB_NEW_MESSAGE 3

typedef CS_ROS_Message * (*CS_ROS_Message_Copy_Constructor)(CS_ROS_Message *);
void cs_ros_matlab_queue_subscribe(CS_ROS_Node_Handle * node_handle, const char * topic, int queue_size,
  CS_ROS_Subscriber_Constructor subscriber_constructor, CS_ROS_Message_Copy_Constructor copy_constructor);
int cs_ros_matlab_spin(CS_ROS_Node_Handle * node_handle);
CS_ROS_Subscriber_Constructor cs_ros_matlab_queue_read_message_type(CS_ROS_Node_Handle * node_handle);
CS_ROS_Message * cs_ros_matlab_queue_read_message(CS_ROS_Node_Handle * node_handle);
void cs_ros_matlab_queue_pop();

#ifdef __cplusplus
}
#endif
