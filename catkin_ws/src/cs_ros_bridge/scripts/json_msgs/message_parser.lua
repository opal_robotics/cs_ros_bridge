
ros_types = {
  bool = "uint8_t",
  int8 = "int8_t",
  uint8 = "uint8_t",
  int16 = "int16_t",
  uint16 = "uint16_t",
  int32 = "int32_t",
  uint32 = "uint32_t",
  int64 = "int64_t",
  uint64 = "uint64_t",
  float32 = "float",
  float64 = "double"}

ros_complex_types = {
  string = "CS_ROS_String",
  time = "CS_ROS_Time",
  duration = "CS_ROS_Duration"
}

function trim(s)
  -- from PiL2 20.4
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function read_message(msg_filename)
  print(msg_filename)
  local message = {};


  local msg_f = io.open(msg_filename, "r");
  local line_no = 1;
  for line in msg_f:lines() do
    line = trim(line)
    --print("line", line_no);

    token_count = 0;

    local member = {arraytype = "none", consttype = "none", valid=false};

    --member_state =

    --print (line);
    local prev_index = 0;
    index = line:find("[%[%]% \t#=]");
    while prev_index ~= #line+1 do

      if index == nil then
        index = #line+1
      end

      if line:sub(index, index) == '#' then
        break;
      end

      if line:sub(index, index) == "=" then
        member["consttype"] = "constant";
      end


      token_begin = line:sub(prev_index, prev_index);
      if token_begin == "" then
        token_begin = "<empty>";
      elseif token_begin == " " then
        token_begin = "<space>";
      elseif token_begin == nil then
        token_begin = "<nil>";
      end

      if index - prev_index == 1 then
        token = "<empty>";
      else
        token = line:sub(prev_index+1, index-1);
      end

      token_end = line:sub(index, index);
      if token_end == "" then
        token_end = "<empty>";
      elseif token_end == " " then
        token_end = "<space>";
      elseif token_end == nil then
        token_end = "<nil>";
      end

      if token_end == "]" then
        if token == "<empty>" then
          member["arraytype"] = "dynamic";
        else
          member["arraytype"] = token;
        end
      elseif token ~= "<empty>" then
        token_count = token_count + 1;
        member[token_count] = token;
        member["valid"] = true;
      end


      --print(token_begin, token, token_end);

      prev_index = index;
      index = line:find("[%[%]% \t#=]", index+1)



    end

    if member.valid then
      member.valid = nil;
      message[member[token_count]] = member;
    end
    line_no = line_no + 1;
  end

  msg_f:close();
  return message;

end


--for i, a in ipairs(arg) do
function get_message_table(message_file, message_namespace)
  local slash_index = message_file:match("^.*()/")
  local dot_index = message_file:find("%.", slash_index);
  --print(slash_index, dot_index);
  local message_name = message_file:sub(slash_index+1, dot_index-1)

  local d = read_message(message_file);
  --print(message_name .. ", {")

  local message = {name = message_name,
    namespace = message_namespace,
    members = {}};

  for k, v in pairs(d) do
    if v[2] == nil then
      print("================= ERRROR ===================================")
      for k2, v2 in pairs(v) do
        print(k2, v2);
      end
      print("============================================================")
      return
    end

    local type = ""
    if ros_types[v[1]] then
      type = ros_types[v[1]]
    elseif ros_complex_types[v[1]] then
      type = ros_complex_types[v[1]]
    else
      type = "void"
    end

    local member = {
      name = v[2],
      type = type,
      arraytype = v.arraytype,
      consttype = v.consttype,
      primitive = ros_types[v[1]] ~= nil
    };

    --io.write("\t" .. v[2] .. "= {\n")
    --io.write("\t\ttype = " .. v[1] .. ",\n")
    --io.write("\t\tname = " .. v[2] .. ",\n")
    --io.write("\t\tarraytype = " .. v.arraytype .. ",\n")
    --io.write("\t\tconsttype = " .. v.consttype .. "\n")
    --if v.consttype ~= "none" then
    --  io.write("\t\tconstvalue = " .. v[3] .. "\n")
    --end
    --if ros_types[v[1]] ~= nil then
    --  io.write("\t\tprimitve = " .. 1 .. "\n");
    --else
    --  io.write("\t\tprimitve = " .. 0 .. "\n");
    --end

    -- io.write("\t}\n")
    message.members[member.name] = member
  end

  return message
  --print("}")
end

function rospkg_get_package_path(package_name)
  return io.popen("python -c \"import rospkg; rospack=rospkg.RosPack(); print rospack.get_path('".. package_name .."')\""):read()
end

return {get_message_table = get_message_table, rospkg_get_package_path = rospkg_get_package_path};

--for k, v in pairs(d) do
--  if v.arraytype == "none" and v.consttype == "none" then
--    io.write(v[1] .. " " .. v[2] .. "\n");
--  elseif v.arraytype ~= "none" then
--     io.write(v[1] .. "[");
--     if v.arraytype ~= "dynamic" then
--       io.write(v.arraytype);
--     end
--     io.write("] " .. v[2] .. "\n");
--   elseif v.consttype ~= "none" then
--     io.write(v[1] .. " " .. v[2] .. " = " .. v[3] .. "\n");
--   end
-- end




--end
