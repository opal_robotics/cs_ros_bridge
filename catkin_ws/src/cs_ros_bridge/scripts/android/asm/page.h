/* This is a stub just to correct a boost bug in this version */
/* see https://issuetracker.google.com/issues/36958770 */

#warning Using stub file instead of real asm/page.h

#if defined(__ANDROID__)
# ifndef PAGE_SIZE
#  define PAGE_SIZE 4096
# endif
#endif
