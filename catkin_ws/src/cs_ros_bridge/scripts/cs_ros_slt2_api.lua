
local slt2 = require('slt2')
package.path = package.path .. ';../?.lua'
json = require('json')

_G = {};
--messages = {};

function cs_ros_api_declaration(return_type, name, args)
  if #args == 0 then
    arg_string = "void"
  else
    arg_string = table.concat(args, ", ");
  end

  return _G["CS_ROS_EXPORT_CONVENTION"] .. " " .. return_type .. " " .. _G["CS_ROS_CALLING_CONVENTION"] .. " " .. name .. " (" ..
    arg_string .. ");";
end

function cs_ros_api_definition(return_type, name, args)
  if #args == 0 then
    arg_string = ""
  else
    arg_string = table.concat(args, ", ");
  end

  return return_type .. " " .. _G["CS_ROS_CALLING_CONVENTION"] .. " " .. name .. " (" ..
    arg_string .. ")";
end

function cs_ros_callback_declaration(return_type, name, args)
  if #args == 0 then
    arg_string = "void"
  else
    arg_string = table.concat(args, ", ");
  end

  return "typedef " .. return_type .. " (" .. _G["CS_ROS_CALLING_CONVENTION"] .. " * " .. name .. ") (" ..
    arg_string .. ");";
end
