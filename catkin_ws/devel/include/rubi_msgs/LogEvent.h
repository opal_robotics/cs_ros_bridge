// Generated by gencpp from file rubi_msgs/LogEvent.msg
// DO NOT EDIT!


#ifndef RUBI_MSGS_MESSAGE_LOGEVENT_H
#define RUBI_MSGS_MESSAGE_LOGEVENT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace rubi_msgs
{
template <class ContainerAllocator>
struct LogEvent_
{
  typedef LogEvent_<ContainerAllocator> Type;

  LogEvent_()
    : header()
    , event_type(0)
    , name()
    , description()  {
    }
  LogEvent_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , event_type(0)
    , name(_alloc)
    , description(_alloc)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef uint32_t _event_type_type;
  _event_type_type event_type;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _name_type;
  _name_type name;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _description_type;
  _description_type description;



  enum {
    INSTANT = 0u,
    BEGIN = 1u,
    END = 2u,
  };


  typedef boost::shared_ptr< ::rubi_msgs::LogEvent_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::rubi_msgs::LogEvent_<ContainerAllocator> const> ConstPtr;

}; // struct LogEvent_

typedef ::rubi_msgs::LogEvent_<std::allocator<void> > LogEvent;

typedef boost::shared_ptr< ::rubi_msgs::LogEvent > LogEventPtr;
typedef boost::shared_ptr< ::rubi_msgs::LogEvent const> LogEventConstPtr;

// constants requiring out of line definition

   

   

   



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::rubi_msgs::LogEvent_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::rubi_msgs::LogEvent_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace rubi_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'rubi_msgs': ['/home/uqsheat3/workspace/opie/rubi-software/catkin_ws/src/rubi_msgs/msg'], 'geometry_msgs': ['/opt/ros/melodic/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::rubi_msgs::LogEvent_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::rubi_msgs::LogEvent_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::rubi_msgs::LogEvent_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::rubi_msgs::LogEvent_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::rubi_msgs::LogEvent_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::rubi_msgs::LogEvent_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::rubi_msgs::LogEvent_<ContainerAllocator> >
{
  static const char* value()
  {
    return "a611093dc2004c63c720c301c60dbe18";
  }

  static const char* value(const ::rubi_msgs::LogEvent_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xa611093dc2004c63ULL;
  static const uint64_t static_value2 = 0xc720c301c60dbe18ULL;
};

template<class ContainerAllocator>
struct DataType< ::rubi_msgs::LogEvent_<ContainerAllocator> >
{
  static const char* value()
  {
    return "rubi_msgs/LogEvent";
  }

  static const char* value(const ::rubi_msgs::LogEvent_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::rubi_msgs::LogEvent_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
\n\
uint32 INSTANT = 0\n\
uint32 BEGIN = 1\n\
uint32 END = 2\n\
\n\
uint32 event_type\n\
\n\
string name\n\
string description\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::rubi_msgs::LogEvent_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::rubi_msgs::LogEvent_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.event_type);
      stream.next(m.name);
      stream.next(m.description);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct LogEvent_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::rubi_msgs::LogEvent_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::rubi_msgs::LogEvent_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "event_type: ";
    Printer<uint32_t>::stream(s, indent + "  ", v.event_type);
    s << indent << "name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.name);
    s << indent << "description: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.description);
  }
};

} // namespace message_operations
} // namespace ros

#endif // RUBI_MSGS_MESSAGE_LOGEVENT_H
