# cs_ros_bridge API #

The cs_ros_bridge API consists of two types of functions - _message agnostic_, and _message dependent_.

## Message agnostic functions ##
---
### cs_ros_argument() ###
```C
void cs_ros_argument (const char * key, const char * value);
```
This function is for passing arguments to ros::init. Generally this is used to pass at least the following:
```C
cs_ros_argument("__master", "http://127.0.0.1:11311");
cs_ros_argument("__ip", "127.0.0.1");
```
cs_ros_argument can only be called *before* cs_ros_init.

---

### cs_ros_init() ###
```C
void cs_ros_init (const char * node_name);
```
This function initialises cs_ros_bridge. Although this function is non-blocking, using it directly requires the following calls:
```C
cs_ros_init("my_node");
CS_ROS_Node_Handle * node_handle = cs_ros_node_handle_create();
```
In this configuration cs_ros_node_handle_create is a blocking function. It is suggested to avoid calling this function directly and use the cs_ros_bgi_* functions instead.

---

### cs_ros_bgi_create() ###
```C
CS_ROS_Background_Initialiser * cs_ros_bgi_create (const char * node_name);
```
The cs_ros_bgi_* functions allow a background initialisation of ROS in an external thread, and as such they do not block (although this function does create an additional thread).

They can be used as follows:
```C
CS_ROS_Background_Initialiser * background_initialiser = cs_ros_bgi_create("my_node");

while (cs_ros_bgi_check(background_initialiser) != CS_ROS_TRUE)
{
  printf("."); // print a dot
  fflush(stdout); // flush
  usleep(1000000); // sleep for a second
}

cs_ros_bgi_join(background_initialiser); // join the background thread
CS_ROS_Node_Handle * node_handle = cs_ros_bgi_get_node_handle(background_initialiser);
cs_ros_bgi_destroy(background_initialiser);
```

---

### cs_ros_bgi_check() ###

```C
uint8_t cs_ros_bgi_check (CS_ROS_Background_Initialiser * background_initialiser);
```

Check whether ROS has finished initialising (i.e. is connected to the ROS master). See cs_ros_bgi_create() for usage.

---

### cs_ros_bgi_get_node_handle() ###

```C
CS_ROS_Node_Handle * cs_ros_bgi_get_node_handle (CS_ROS_Background_Initialiser * background_initialiser);
```

Retrieves the node handle that is used for background initialisation. Note: if this function is not used, the memory for the node handle will not be freed, so call this function if using background initialisation. See cs_ros_bgi_create() for usage.

The returned node handle must be freed with cs_ros_node_handle_destroy().

---

### cs_ros_bgi_join() ###

```C
void cs_ros_bgi_join (CS_ROS_Background_Initialiser * background_initialiser);
```

Join the background initialisation thread to stop it disappearing into the aether. See cs_ros_bgi_create() for usage.

---

### cs_ros_bgi_destroy() ###

```C
void cs_ros_bgi_destroy (CS_ROS_Background_Initialiser * background_initialiser);
```

Destroy the background initialiser when finished. This does not destroy the created node handle. See cs_ros_bgi_create for usage.

---

### cs_ros_check_master() [Blocking] ###

```C
uint8_t cs_ros_check_master (void);
```

This function checks for the ROS master and can be called before creating a node handle. It is a wrapper around ros::check_master and takes around 1 sec to timeout, so it is not recommended for use. Use the cs_ros_bgi_* functions instead.

---

### cs_ros_node_handle_create() [Blocking] ###

```C
CS_ROS_Node_Handle *  cs_ros_node_handle_create (void);
```

This function blocks on the first call until ROS is connected. It is better to avoid it and use the cs_ros_bgi_* functions instead. Once ROS is initialised, the function will not block when called again.

---

### cs_ros_node_handle_destroy() ###

```C
void  cs_ros_node_handle_destroy (CS_ROS_Node_Handle * node_handle);
```

Destroy a node handle created by cs_ros_node_handle_create() or cs_ros_bgi_get_node_handle().

---

### cs_ros_spin_once() ###

```C
CS_ROS_Bool  cs_ros_spin_once (CS_ROS_Node_Handle * node_handle);
```

A wrapper around ros::ok() and ros::spinOnce(). Call this function repeatedly in a loop in order to receive ROS message callbacks.

```C
while (cs_ros_spin_once(node_handle) == CS_ROS_TRUE)
{
  usleep(10000); // sleep for 10ms
}
// if cs_ros_spin_once returns CS_ROS_FALSE, it means that we've been told
// to shutdown.
```

cs_ros_spin_once() returns either CS_ROS_TRUE for running normally, or CS_ROS_FALSE if the node has been asked to shut down.

cs_ros_spin_once() will call message callbacks if there were messages received since the last call.

---

### cs_ros_subscribe() ###

```C
CS_ROS_Subscriber *  cs_ros_subscribe (
  CS_ROS_Node_Handle * node_handle,
  const char * topic, int queue_size,
  CS_ROS_Message_Callback callback, void * user_data,
  CS_ROS_Subscriber_Constructor subscriber_constructor);
```

cs_ros_subscribe() subscribes to a topic and provides a callback and message type (through subscriber_constructor). Example usage:

```C
void my_amazing_topic_callback(CS_ROS_Message * message, void * user_data);

CS_ROS_Subscriber * my_amazing_topic subscriber = cs_ros_subscribe(
  node_handle, // the node handle
  "/my/amazing/topic", // topic name
  1, // queue_size, where 0 is infinite
  &my_amazing_topic_callback, // pointer to the callback
  NULL, // an extra pointer to pass to the callback
  &cs_ros_message_String_subscriber_ctor // the message type through its
                                        // subscriber contstructor.
);
```

The CS_ROS_Subscriber_Constructor is unique for each message type, and are generated from the message files (see the message dependent API). They always take the form cs_ros_message_{MESSAGE_TYPE}_subscriber_ctor.

---

### cs_ros_unsubscribe() ###

```C
void  cs_ros_unsubscribe (CS_ROS_Subscriber * subscriber);
```

Unsubscribe from a topic, and delete the subscriber.

---

### cs_ros_advertise() ###

```C
CS_ROS_Publisher *  cs_ros_advertise (
  CS_ROS_Node_Handle * node_handle,
  const char * topic, int queue_size,
  CS_ROS_Publisher_Constructor constructor);
```

Advertise a topic. Example usage:

```C
CS_ROS_Publisher * my_amazing_topic_publisher = cs_ros_advertise (
  node_handle, // the node handle
  "/my/amazing/topic", // the topic name
  1, // outgoing queue size, where 0 is infinite
  &cs_ros_String_publisher_ctor // the message type through its
                                // publisher constructor
);
```

The CS_ROS_Publisher_Constructor is unique for each message type, and are generated from the message files (see the message dependent API). They always take the form cs_ros_message_{MESSAGE_TYPE}_publisher_ctor.

---

### cs_ros_unadvertise() ###

```C
void  cs_ros_unadvertise (CS_ROS_Publisher * publisher);
```

Unadvertise a topic and delete the publisher.

---

### cs_ros_publish() ###

```C
void  cs_ros_publish (CS_ROS_Publisher * publisher, CS_ROS_Message * message);
```

Publish a message on a topic. Example usage:

```C
// create a string message
CS_ROS_Message * my_amazing_msg = cs_ros_message_String_create();

// get pointer to the ROS message
void * my_amazing_msg_ptr = cs_ros_message_String_get_pointer(my_amazing_msg);

// get a pointer to std_msgs::String::data member
// it has type CS_ROS_String *
CS_ROS_String * string_ptr = cs_ros_message_String_get_pointer_data(my_amazing_msg_ptr);

// set the CS_ROS_String inside the message
cs_ros_string_set(string_ptr, "this message is amazing");

// publish the message
cs_ros_publish(my_amazing_topic_publisher, my_amazing_msg);

// the message can now safely be destroyed (the ROS Message is contained in a shared pointer)
cs_ros_message_String_destroy(my_amazing_msg);
```
More information about message manipulation can be seen in the message dependent API.

Messages can only be re-used if they are not sent intra-process. In the intra-process case, messages are not copied, instead the pointer is transferred directly to the callback.

---

### cs_ros_time_now_sec() ###

```C
double  cs_ros_time_now_sec (void);
```

This is equivalent to calling ros::Time::now().to_sec(). It returns time since the epoch as a floating-point number.

---

### cs_ros_time_now() ###

```C
void  cs_ros_time_now (unsigned int * sec, unsigned int * nsec);
```

This is equivalent to calling:

```C
ros::Time time = ros::Time::now();
sec = time.sec;
nsec = time.nsec;
```

Example usage:
```C
int sec, nsec;
cs_ros_time_now(&sec, &nsec);
```

---

### cs_ros_string_get() ###

```C
const char *  cs_ros_string_get (CS_ROS_String * s);
```

Get a C-style string from CS_ROS_String.

---

### cs_ros_string_set() ###

```C
void  cs_ros_string_set (CS_ROS_String * s, const char * value);
```

Set a ROS string from a C-style string.

---

### cs_ros_string_destroy() ###

```C
void  cs_ros_string_destroy (CS_ROS_String * s);
```

Destroy a CS_ROS_String. I have no idea what this function was intended for.

---

### cs_ros_time_get_sec() ###

```C
uint32_t  cs_ros_time_get_sec (CS_ROS_Time * t);
```

Get the seconds from CS_ROS_Time pointer.

---

### cs_ros_time_get_nsec() ###

```C
uint32_t  cs_ros_time_get_nsec (CS_ROS_Time * t);
```

Get the nanoseconds from a CS_ROS_Time pointer.

---

### cs_ros_time_set_sec() ###

```C
void  cs_ros_time_set_sec (CS_ROS_Time * t, uint32_t value);
```

Set the seconds in a CS_ROS_Time pointer.

---

### cs_ros_time_set_nsec() ###

```C
void  cs_ros_time_set_nsec (CS_ROS_Time * t, uint32_t value);
```

Set the nanoseconds in a CS_ROS_Time pointer.

---

### cs_ros_time_set_now ###

```C
void  cs_ros_time_set_now (CS_ROS_Time * t);
```

Set a CS_ROS_Time pointer to the current time.

---

### cs_ros_time_to_sec() ###

```C
double  cs_ros_time_to_sec (CS_ROS_Time * t);
```

Convert a CS_ROS_Time pointer into floating-point seconds. Equivalent to ros::Time::to_sec().

---

### cs_ros_time_to_nsec() ###

```C
uint64_t  cs_ros_time_to_nsec (CS_ROS_Time * t);
```

Convert a CS_ROS_Time pointer into nanoseconds.

---

### cs_ros_duration_get_sec() ###

```C
int32_t  cs_ros_duration_get_sec (CS_ROS_Duration * t);
```

Get seconds from a CS_ROS_Duration pointer.

---

### cs_ros_duration_get_nsec() ###

```C
int32_t  cs_ros_duration_get_nsec (CS_ROS_Duration * t);
```

Get nanoseconds from a CS_ROS_Duration pointer.

---

### cs_ros_duration_set_sec() ###

```C
void  cs_ros_duration_set_sec (CS_ROS_Duration * t, int32_t value);
```

Set the seconds component of a CS_ROS_Duration pointer.

---

### cs_ros_duration_set_nsec() ###

```C
void  cs_ros_duration_set_nsec (CS_ROS_Duration * t, int32_t value);
```

Set the nanoseconds component of a CS_ROS_Duration pointer.

---

### cs_ros_duration_to_sec() ###

```C
double  cs_ros_duration_to_sec (CS_ROS_Duration * t);
```

Convert a CS_ROS_Duration pointer to floating-point seconds.

---

### cs_ros_duration_to_nsec() ###

```C
int64_t  cs_ros_duration_to_nsec (CS_ROS_Duration * t);
```

Convert a CS_ROS_Duration pointer to nanoseconds.

---

### cs_ros_param_set() [Blocking] ###

```C
void  cs_ros_param_set (const char * key, const char * value);
```

Set a ROS parameter. This function will block until the parameter is set.

---

### cs_ros_param_get() [Blocking] ###

```C
CS_ROS_String *  cs_ros_param_get (const char * key);
```

Get a ROS parameter. This function will block until the parameter is returned. If the parameter is not set, it will return an empty string.

## Message dependent functions ##

Message dependent functions are created for every message, and include *_create and *_destroy, along with access to all the message members.

In the following functions, the "*" indicates the message name. For example, for the std_msgs/String message type, the functions cs_ros_message_String_create(), and cs_ros_message_String_destroy() would be generated.

### cs_ros_message_*_create() ###

```C
CS_ROS_Message * cs_ros_message_*_create(void);
```

Creates a new message.

---

### cs_ros_message_*_destroy() ###

```C
void cs_ros_message_*_destroy(CS_ROS_Message * message);
```

Destroys a message.

---

### cs_ros_message_*_copy() ###

```C
CS_ROS_Message * cs_ros_message_*_copy(CS_ROS_Message * message_to_copy);
```

Makes a shallow copy of a message (for the default implementation only the pointer is copied). The main purpose of this function is to allow retaining a message from within a message callback, and to enable use in languages that do not support callbacks (e.g., MATLAB).

---

### cs_ros_message_*_publisher_ctor ###

```C
CS_ROS_Publisher_Constructor cs_ros_message_*_publisher_ctor;
```

A handle that represents a publisher constructor for a particularly message type. Used as an argument to cs_ros_advertise() (see cs_ros_advertise() above).

---

### cs_ros_message_*_subscriber_ctor ###

```C
CS_ROS_Subscriber_Constructor cs_ros_message_*_subscriber_ctor;
```

A handle that represents a subscriber constructor for a particularly message type. Used as an argument to cs_ros_subscribe() (see cs_ros_subscribe() above).

---

### cs_ros_message_*_get_pointer() ###

```C
void * cs_ros_message_*_get_pointer(CS_ROS_Message * message);
```

Get a raw pointer to the message. Under the hood, this function returns a pointer to the ROS message, and is required to access members from a CS_ROS_Message struct. This function should only be used on CS_ROS_Message structs, not on any pointers returned by *_get_pointer(), *_get_pointer_*(), *_get_array_pointer_*() or *_get_array_member_pointer_*().

## Message member dependent functions

These functions are dependent on a messages member functions. In the following descriptions, the first "\*" indicates the message type, the second "\*" indicates the member name, and the "type(*)" indicates the member type.

### cs_ros_message_*_get\_\*() ###

```C
type(*) cs_ros_message_*_get_*(void * message_pointer);
type(*) cs_ros_message_*_get_*(); // for constant members
```

Get a member from a message. For example, for the std_msgs/Int32 message, for the member "data", this function is as follows:

```C
int32_t cs_ros_message_Int32_get_data(void * message_pointer);
```

This function only exists for primitive types (i.e., types that do not include string, time, duration or nested messages).

---

### cs_ros_message_*_set\_\*() ###

```C
void cs_ros_message_*_set_*(void * message_pointer, type(*) value);
```

Set a member in a message. For example, for the std_msgs/Int32 message, for the member "data", this function is as follows:

```C
void cs_ros_message_Int32_get_data(void * message_pointer, int32_t value);
```

This function only exists for primitive types (i.e., types that do not include string, time, duration or nested messages).

---

### cs_ros_message_*_get_pointer\_\*() ###

```C
type(*) * cs_ros_message_*_get_pointer_*(void * message_pointer);
```
Get a pointer to the member. This function is the only way to access non-primitive types, and nested messages. Modifying the pointer will modify the copy of the image. For example, changing the Header of a message:

```C
// create a new message
CS_ROS_Message * my_message = cs_ros_message_BatteryState_create();

// get a pointer to the message
void * my_message_ptr = cs_ros_message_BatteryState_get_pointer(my_message);

// get a pointer to the header of the message
// (note: no set function exists for the non-primitive header)
void * header_ptr = cs_ros_message_BatteryState_get_pointer_header(my_message_ptr);

// get a pointer to the header stamp
// (note: no set function exists for the non-primitive stamp)
CS_ROS_Time * stamp_ptr = cs_ros_message_Header_get_pointer_stamp(my_message_ptr);

// set the header stamp to the current time
cs_ros_time_set_now(stamp_ptr);

// set the header sequence to the current
// (note: set function exists for seq, because seq is primitive uint32_t)
cs_ros_message_Header_set_seq(header_ptr, current_seq);

// publish message
cs_ros_publish(my_publisher, my_message);
```

---

### cs_ros_message_*_get_array_pointer\_\*() ###

```C
type(*) * cs_ros_message_*_get_pointer_*(void * message_pointer);
```
Get a raw pointer to a continuous array. This function is only useful for primitive types, where the size of the type can be calculated. For non-primitive types it returns a void* pointer.

This function only exists for array members.

---

### cs_ros_message_*_get_size\_\*() ###

```C
uint32_t cs_ros_message_*_get_size_*(void * message_pointer);
```

Get the size of an array member (number of elements, _not_ the size in bytes).

This function only exists for array members.

---

### cs_ros_messae_*_resize\_\*() ###

```C
void cs_ros_message_*_resize_*(void * message_pointer, uint32_t size);
```

Resize an array member. In roscpp messages, array members are represented using std::vector. This function will call std::vector::resize() under the hood.

This function only exists for array members.

---

### cs_ros_message_*_get_array_member\_\*() ###

```C
type(*) cs_ros_message_*_get_array_member_*(void * message_pointer, int index);
```
Similar to cs_ros_message_*_get\_\*() above - get element at index in a member array.

This function only exists for array members, and only if the array member's type is primitive.

---

### cs_ros_message_*_set_array_member\_\*() ###

```C
void cs_ros_message_*_set_array_member_*(void * message_pointer, int index, type(*) value);
```

Similar to cs_ros_message_*_set\_\*() above - set element at index in a member array.

This function only exists for array members, and only if the array member's type is primitive.

---

### cs_ros_message_*_get_array_member_pointer\_\*() ###

Similar to cs_ros_message_*_get_pointer\_\*() above - get pointer to element at index in a member array.

This function is the only option for non-primmitive array elements.

```C
type(*) * cs_ros_message_*_get_array_member_pointer_*(void * message_pointer, int index) }#
```
